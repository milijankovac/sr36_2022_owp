package com.travelagency.travelAgency.controller;

import com.travelagency.travelAgency.model.Korisnik;
import com.travelagency.travelAgency.model.Putovanje;
import com.travelagency.travelAgency.model.Rezervacija;
import com.travelagency.travelAgency.model.StatusRezervacije;
import com.travelagency.travelAgency.service.KategorijaPutovanjaService;
import com.travelagency.travelAgency.service.PutovanjeService;
import com.travelagency.travelAgency.service.RezervacijaService;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "rezervacije")
public class RezervacijeController implements ServletContextAware {
    public static final String KORISNIK_KEY = "prijavljeniKorisnik";
    @Autowired
    private KategorijaPutovanjaService kategorijaPutovanjaService;
    @Autowired
    private PutovanjeService putovanjeService;
    @Autowired
    private RezervacijaService rezervacijaService;
    @Autowired
    private ServletContext servletContext;
    private  String bURL;
    @PostConstruct
    public void init() {
        bURL = servletContext.getContextPath()+"/";
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    @GetMapping
    public ModelAndView getRezervacijeByKorisnik(HttpSession session, @RequestParam(value = "status", required = false) String status) {
        Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if (korisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }

        List<Rezervacija> rezervacije;


        if ("finished".equals(status)) {
            rezervacije = rezervacijaService.getFinishedRezervacijeByKorisnik(korisnik.getId());
        } else if ("upcoming".equals(status)) {
            rezervacije = rezervacijaService.getUpcomingRezervacijeByKorisnik(korisnik.getId());
        } else {
            rezervacije = rezervacijaService.getRezervacijeByKorisnik(korisnik.getId());
        }

        ModelAndView modelAndView = new ModelAndView("rezervacije");
        modelAndView.addObject("rezervacije", rezervacije);
        modelAndView.addObject("successMessage", null);
        modelAndView.addObject("errorMessage", null);
        modelAndView.addObject("session", session.getAttribute(KorisnikController.KORISNIK_KEY));
        session.setAttribute("korisnik", korisnik);
        return modelAndView;
    }


    @PostMapping(value = "/rezervisi/{id}")
    public String rezervisiPutovanje(@PathVariable Long id,
                                     @RequestParam("brojMesta") int brojMesta,
                                     HttpSession session,
                                     RedirectAttributes redirectAttributes) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().name().equals("KUPAC")) {
            return "redirect:/korisnici/login";
        }

        // Dodajte provjeru da li ima dovoljno slobodnih mjesta prije rezervacije
        Putovanje putovanje = putovanjeService.getById(id);
        if (putovanje == null) {
            // Handle error if putovanje is null (not found)
            redirectAttributes.addFlashAttribute("errorMessage", "Putovanje nije pronađeno.");
            return "redirect:/putovanja";
        }

        if (putovanje.getBrojSlobodnihMesta() < brojMesta) {
            // Handle error if nema dovoljno slobodnih mjesta
            redirectAttributes.addFlashAttribute("errorMessage", "Nema dovoljno slobodnih mjesta za rezervaciju " + brojMesta + " mjesta.");
            return "redirect:/putovanja/" + id;
        }


        boolean rezervisano = rezervacijaService.reserve(id, prijavljeniKorisnik.getId(), brojMesta);

        if (rezervisano) {
            redirectAttributes.addFlashAttribute("successMessage", "Uspešno ste rezervisali putovanje.");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Neuspešna rezervacija. Možda je došlo do greške.");
        }

        return "redirect:/putovanja/" + id;
    }

    @PostMapping(value = "/otkazi/{id}")
    public ModelAndView otkaziRezervaciju(@PathVariable Long id, HttpSession session, RedirectAttributes redirectAttributes) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }

        Rezervacija rezervacija = rezervacijaService.findById(id);
        if (rezervacija == null) {
            redirectAttributes.addFlashAttribute("errorMessage", "Rezervacija ne postoji.");
            return new ModelAndView("redirect:/rezervacije");
        }

        Putovanje putovanje = rezervacija.getPutovanje();
        LocalDateTime sada = LocalDateTime.now();
        LocalDateTime vremePolaska = putovanje.getDatumVremePolaska();

        if (vremePolaska == null) {
            redirectAttributes.addFlashAttribute("errorMessage", "Vreme polaska putovanja nije definisano.");
            return new ModelAndView("redirect:/rezervacije");
        }

        if (Duration.between(sada, vremePolaska).toHours() < 48) {
            redirectAttributes.addFlashAttribute("errorMessage", "Ne možete otkazati rezervaciju manje od 48 sati pre polaska.");
            return new ModelAndView("redirect:/rezervacije");
        }

        boolean otkazano = rezervacijaService.cancel(id);

        if (otkazano) {
            redirectAttributes.addFlashAttribute("successMessage", "Uspešno ste otkazali rezervaciju.");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Neuspešno otkazivanje rezervacije.");
        }

        return new ModelAndView("redirect:/rezervacije");
    }
    @GetMapping("/sve")
    public ModelAndView prikaziSveRezervacije(HttpSession session) {
        Korisnik korisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (korisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }


        List<Rezervacija> sveRezervacije = rezervacijaService.getAll();

        ModelAndView modelAndView = new ModelAndView("sveRezervacije");
        modelAndView.addObject("rezervacije", sveRezervacije);
        modelAndView.addObject("session", session.getAttribute("prijavljeniKorisnik"));

        return modelAndView;
    }
    @PostMapping("/prihvati")
    public ModelAndView potvrdiRezervaciju(@RequestParam("rezervacijaId") Long rezervacijaId, HttpSession session) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }

        // Implementacija potvrde rezervacije (servisni poziv)
        boolean uspesno = rezervacijaService.prihvatiRezervaciju(rezervacijaId);

        // Redirekcija na prikaz svih rezervacija
        return new ModelAndView("redirect:/rezervacije/sve");
    }

    @PostMapping("/odbij")
    public ModelAndView odbijRezervaciju(@RequestParam("rezervacijaId") Long rezervacijaId, HttpSession session) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }

        // Implementacija odbijanja rezervacije (servisni poziv)
        boolean uspesno = rezervacijaService.odbijRezervaciju(rezervacijaId);

        // Redirekcija na prikaz svih rezervacija
        return new ModelAndView("redirect:/rezervacije/sve");
    }
}
