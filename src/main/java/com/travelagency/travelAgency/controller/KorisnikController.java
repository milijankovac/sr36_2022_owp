package com.travelagency.travelAgency.controller;


import com.travelagency.travelAgency.model.Korisnik;
import com.travelagency.travelAgency.model.Uloga;
import com.travelagency.travelAgency.service.KorisnikService;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/korisnici")
public class KorisnikController implements ServletContextAware {
    public static final String KORISNIK_KEY = "prijavljeniKorisnik";
    @Autowired
    private ServletContext servletContext;
    private  String bURL;
    @Autowired
    private KorisnikService korisnikService;
    @PostConstruct
    public void init() {
        bURL = servletContext.getContextPath()+"/";
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    @GetMapping(value = "/login")
    public ModelAndView getLogin(){
        ModelAndView retVal = new ModelAndView("login");
        retVal.addObject("login", korisnikService.getAll());
        return retVal;
    }
    @PostMapping(value="/login")
    public ModelAndView postLogin(@RequestParam String email, @RequestParam String lozinka,
                                  HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException {
        try {
            // validacija
            Korisnik korisnik = korisnikService.findForLogin(email, lozinka);
            if (korisnik == null) {
                throw new Exception("Neispravno korisničko ime ili lozinka!");
            }

            // prijava
            session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
            Korisnik korisnik1 = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
            System.out.println("Prijavljeni korisnik:" + korisnik1.getIme());
            System.out.println("Uloga: " + korisnik1.getUloga());

            response.sendRedirect(bURL);
            return null;
        } catch (Exception ex) {
            // ispis greške
            String poruka = ex.getMessage();
            if (poruka == null || poruka.isEmpty()) {
                poruka = "Neuspešna prijava!";
            }

            // prosleđivanje
            ModelAndView rezultat = new ModelAndView("login");
            rezultat.addObject("poruka", poruka);

            return rezultat;
        }
    }

    @GetMapping(value="/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().removeAttribute(KORISNIK_KEY);
        request.getSession().invalidate();
        response.sendRedirect( "/travelagency/korisnici/login");
    }

    // GET metoda za prikazivanje stranice za registraciju
    @GetMapping(value = "/registracija")
    public ModelAndView getRegistracija() {
        ModelAndView retVal = new ModelAndView("registracija");
        retVal.addObject("registracija", korisnikService.getAll());
        return retVal;
    }
    @PostMapping(value = "/registracija")
    public ModelAndView registracija(
            @RequestParam String email,
            @RequestParam String lozinka,
            @RequestParam String potvrdiLozinku,
            @RequestParam String ime,
            @RequestParam String prezime,
            @RequestParam String datumRodjenja,
            @RequestParam String jmbg,
            @RequestParam String adresa,
            @RequestParam String brojTelefona,
            RedirectAttributes redirectAttributes) {

        // Proverava da li se lozinke podudaraju
        if (!lozinka.equals(potvrdiLozinku)) {
            redirectAttributes.addAttribute("error", "Lozinke se ne podudaraju.");
            return new ModelAndView("redirect:/korisnici/registracija");
        }

        // Konvertovanje stringova u odgovarajuće tipove
        LocalDate parsedDatumRodjenja = LocalDate.parse(datumRodjenja);
        Integer parsedBrojTelefona = Integer.parseInt(brojTelefona);
        LocalDateTime datumVremeRegistracije = LocalDateTime.now();
        Uloga uloga = Uloga.KUPAC;

        Korisnik korisnik = new Korisnik(email, lozinka, ime, prezime, parsedDatumRodjenja, jmbg, adresa, parsedBrojTelefona, datumVremeRegistracije, uloga);
        // Kreiranje korisnika sa trenutnim datumom i vremenom registracije
        korisnikService.create(korisnik);

        // Redirektovanje nakon registracije
        redirectAttributes.addFlashAttribute("success", "Uspešno ste se registrovali. Možete se prijaviti.");
        return new ModelAndView("redirect:/korisnici/login");
    }

    @PostMapping(value="/obrisi")
    public void delete(@RequestParam Long id,
                       HttpSession session, HttpServletResponse response) throws IOException {
//         Autentikacija, autorizacija
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);

		// Samo administrator može da briše korisnike, ali ne i sebe
		if (prijavljeniKorisnik == null  || prijavljeniKorisnik.getId().equals(id)) {
			response.sendRedirect(bURL + "Korisnici");
			return;
		}

        // Brisanje
        korisnikService.delete(id);

        response.sendRedirect(bURL + "korisnici");
    }

    @GetMapping
    @ResponseBody
    public ModelAndView getKorisnici(HttpSession session){
        List<Korisnik> korisnici = korisnikService.getAll();
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }

        // podaci sa nazivom template-a
        ModelAndView rezultat = new ModelAndView("korisnici"); // naziv template-a
        rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

        return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Korisnik>> getAll() {
        List<Korisnik> korisnici = korisnikService.getAll();
        return new ResponseEntity<>(korisnici, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Korisnik> get(@PathVariable("id") Long id) {
        Korisnik korisnik = korisnikService.findById(id);
        if (korisnik != null) {
            return new ResponseEntity<>(korisnik, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createUser(
            @RequestParam String email,
            @RequestParam String lozinka,
            @RequestParam String ime,
            @RequestParam String prezime,
            @RequestParam String datumRodjenja,
            @RequestParam String jmbg,
            @RequestParam String adresa,
            @RequestParam Integer brojTelefona,
            @RequestParam Uloga uloga) {
        try {
            LocalDate parsedDatumRodjenja = LocalDate.parse(datumRodjenja);
            LocalDateTime datumVremeRegistracije = LocalDateTime.now();
            Uloga userUloga = Uloga.KUPAC;

            if (korisnikService.findByUsername(email) != null) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            Korisnik korisnik = new Korisnik(email,lozinka,ime,prezime,parsedDatumRodjenja,jmbg,adresa,brojTelefona,datumVremeRegistracije,uloga);
            // Kreiranje korisnika sa trenutnim datumom i vremenom registracije
            korisnikService.create(korisnik);
            return new ResponseEntity<>("User is successfully created.", HttpStatus.OK);

        } catch (DateTimeParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>("Invalid role provided.", HttpStatus.BAD_REQUEST);
        }
    }
    //Endpint za izmjenu korisnika
    @GetMapping("/izmjeniKorisnika/{id}")
    public ModelAndView prikaziFormuZaIzmjenu(@PathVariable("id") Long id,HttpSession session) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }
        ModelAndView modelAndView = new ModelAndView("izmjeniKorisnika");
        Korisnik korisnik = korisnikService.findById(id);
        modelAndView.addObject("korisnik", korisnik);
        return modelAndView;
    }
    @PostMapping(value = "/sacuvajIzmjene", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView updateUser(
            @RequestParam Long id,
            @RequestParam String email,
            @RequestParam String lozinka,
            @RequestParam String ime,
            @RequestParam String prezime,
            @RequestParam String datumRodjenja,
            @RequestParam String jmbg,
            @RequestParam String adresa,
            @RequestParam Integer brojTelefona) {

        try {
            LocalDate parsedDatumRodjenja = LocalDate.parse(datumRodjenja);
            Korisnik existingKorisnik = korisnikService.findById(id);

            if (existingKorisnik == null) {
                ModelAndView modelAndView = new ModelAndView("errorPage");
                modelAndView.addObject("errorMessage", "Korisnik nije pronađen.");
                return modelAndView;
            }

            existingKorisnik.setEmail(email);
            existingKorisnik.setLozinka(lozinka);
            existingKorisnik.setIme(ime);
            existingKorisnik.setPrezime(prezime);
            existingKorisnik.setDatumRodjenja(parsedDatumRodjenja);
            existingKorisnik.setJmbg(jmbg);
            existingKorisnik.setAdresa(adresa);
            existingKorisnik.setBrojTelefona(brojTelefona);

            korisnikService.update(existingKorisnik);

            ModelAndView modelAndView = new ModelAndView("redirect:/korisnici");
            return modelAndView;

        } catch (DateTimeParseException e) {
            ModelAndView modelAndView = new ModelAndView("errorPage");
            modelAndView.addObject("errorMessage", "Neuspjelo parsiranje datuma.");
            return modelAndView;
        } catch (IllegalArgumentException e) {
            ModelAndView modelAndView = new ModelAndView("errorPage");
            modelAndView.addObject("errorMessage", "Nevažeći argument.");
            return modelAndView;
        }
    }



    @GetMapping(value = "/profil")
    public ModelAndView getProfil(HttpSession session) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }
        ModelAndView modelAndView = new ModelAndView("profil");
        modelAndView.addObject("korisnik", prijavljeniKorisnik);
        return modelAndView;
    }
    // Endpoint za promjenu lozinke
    @PostMapping(value = "/promeniLozinku", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ModelAndView promeniLozinku(
            @RequestParam String currentPassword,
            @RequestParam(required = false) String newPassword,
            @RequestParam(required = false) String confirmNewPassword,
            HttpSession session,
            RedirectAttributes redirectAttributes) {

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);

        if (prijavljeniKorisnik == null) {
            redirectAttributes.addFlashAttribute("error", "Korisnik nije prijavljen.");
            return new ModelAndView("redirect:/korisnici/login");
        }

        if (newPassword == null || newPassword.isEmpty()) {
            // Ako nova lozinka nije uneta, ostaje nepromenjena
            redirectAttributes.addFlashAttribute("success", "Lozinka nije promenjena jer nova lozinka nije uneta.");
            return new ModelAndView("redirect:/korisnici/profil");
        }

        if (!newPassword.equals(confirmNewPassword)) {
            redirectAttributes.addFlashAttribute("error", "Lozinke se ne podudaraju.");
            return new ModelAndView("redirect:/korisnici/profil");
        }

        if (!korisnikService.checkPassword(prijavljeniKorisnik, currentPassword)) {
            redirectAttributes.addFlashAttribute("error", "Trenutna lozinka je neispravna.");
            return new ModelAndView("redirect:/korisnici/profil");
        }

        try {
            boolean uspeh = korisnikService.updatePassword(prijavljeniKorisnik, newPassword);
            if (!uspeh) {
                redirectAttributes.addFlashAttribute("error", "Došlo je do greške prilikom promene lozinke.");
            } else {
                redirectAttributes.addFlashAttribute("success", "Lozinka je uspešno promenjena.");
            }
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", "Došlo je do greške prilikom promene lozinke.");
        }

        return new ModelAndView("redirect:/korisnici/profil");
    }


    @PostMapping("/izmjeni")
    public ModelAndView updateUser(
            HttpSession session,
            @RequestParam("ime") String ime,
            @RequestParam("prezime") String prezime,
            @RequestParam("datumRodjenja") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate datumRodjenja,
            @RequestParam("jmbg") String jmbg,
            @RequestParam("adresa") String adresa,
            @RequestParam("brojTelefona") Integer brojTelefona,
            RedirectAttributes redirectAttributes) {

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        if (prijavljeniKorisnik != null) {
            // Ažuriraj podatke korisnika
            prijavljeniKorisnik.setIme(ime);
            prijavljeniKorisnik.setPrezime(prezime);
            prijavljeniKorisnik.setDatumRodjenja(datumRodjenja);
            prijavljeniKorisnik.setJmbg(jmbg);
            prijavljeniKorisnik.setAdresa(adresa);
            prijavljeniKorisnik.setBrojTelefona(brojTelefona);

            // Sačuvaj izmene u bazi
            korisnikService.update(prijavljeniKorisnik);

            // Osveži podatke u sesiji
            session.setAttribute(KORISNIK_KEY, prijavljeniKorisnik);

            redirectAttributes.addFlashAttribute("success", "Podaci su uspešno ažurirani.");
        } else {
            redirectAttributes.addFlashAttribute("error", "Došlo je do greške prilikom ažuriranja podataka.");
        }

        return new ModelAndView("redirect:/korisnici/profil");
    }
}
