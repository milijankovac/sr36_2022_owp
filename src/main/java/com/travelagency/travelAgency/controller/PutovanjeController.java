package com.travelagency.travelAgency.controller;


import com.travelagency.travelAgency.model.*;
import com.travelagency.travelAgency.service.KategorijaPutovanjaService;
import com.travelagency.travelAgency.service.PutovanjeService;
import com.travelagency.travelAgency.service.RezervacijaService;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping(value = "putovanja")
public class PutovanjeController implements ServletContextAware {
    public static final String KORISNIK_KEY = "prijavljeniKorisnik";
    @Autowired
    private KategorijaPutovanjaService kategorijaPutovanjaService;
    @Autowired
    private RezervacijaService rezervacijaService;
    public static final String PUTOVANJE_KEY = "putovanje";
    @Autowired
    private ServletContext servletContext;
    private  String bURL;
    @PostConstruct
    public void init() {
        bURL = servletContext.getContextPath()+"/";
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    @Autowired
    private PutovanjeService putovanjeService;
    @Autowired
    public PutovanjeController(KategorijaPutovanjaService kategorijaPutovanjaService) {
        this.kategorijaPutovanjaService = kategorijaPutovanjaService;
    }

    //Get all travels for /putovanja path
    @GetMapping
    public ModelAndView getPutovanja() {
        ModelAndView modelAndView = new ModelAndView("putovanja");
        modelAndView.addObject("putovanja", putovanjeService.getAll());
        return modelAndView;
    }

//    @GetMapping("/pretraga")
//    public ModelAndView search(
//            @RequestParam(value = "prevoznoSredstvo", required = false) String prevoznoSredstvo,
//            @RequestParam(value = "nazivDestinacije", required = false) String nazivDestinacije,
//            @RequestParam(value = "kategorijaPutovanja", required = false) Long kategorijaId,
//            @RequestParam(value = "brojNocenja", required = false) Integer brojNocenja,
//            @RequestParam(value = "minCena", required = false) Double minCena,
//            @RequestParam(value = "maxCena", required = false) Double maxCena) {
//        List<Putovanje> putovanja = putovanjeService.searchPutovanja(prevoznoSredstvo, nazivDestinacije, kategorijaId, brojNocenja, minCena, maxCena);
//        ModelAndView mav = new ModelAndView("index");  // naziv Thymeleaf šablona (rezultatiPretrage.html)
//        mav.addObject("putovanja", putovanja);
//        return mav;
//    }
//
//    @GetMapping("/sortiraj")
//    public ModelAndView sort(
//            @RequestParam(value = "sortirajPo", required = false) String sortBy) {
//        List<Putovanje> putovanja = putovanjeService.sortPutovanja(sortBy);
//        ModelAndView mav = new ModelAndView("index");  // naziv Thymeleaf šablona (rezultatiSortiranja.html)
//        mav.addObject("putovanja", putovanja);
//        return mav;
//    }

    @GetMapping(value = "/{id}")
    public ModelAndView getById(@PathVariable Long id, HttpSession session) {
        Putovanje putovanje = putovanjeService.getById(id);
        ModelAndView modelAndView = new ModelAndView("detaljiPutovanja");
        modelAndView.addObject("putovanje", putovanje);
        modelAndView.addObject("cenaSaPopustom", putovanje.getCenaSaPopustom());

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }

        if (prijavljeniKorisnik != null && prijavljeniKorisnik.getUloga().name().equals("KUPAC")) {
            List<Rezervacija> rezervacije = rezervacijaService.existsReservation(id, prijavljeniKorisnik.getId());

            boolean nemaNaCekanju = rezervacije.stream().noneMatch(r -> r.getStatusRezervacije() == StatusRezervacije.NA_CEKANJU);
//            boolean imaOdbijenih = rezervacije.stream().anyMatch(r -> r.getStatusRezervacije() == StatusRezervacije.ODBIJENO);
            boolean imaPrihvacenih = rezervacije.stream().anyMatch(r -> r.getStatusRezervacije() == StatusRezervacije.PRIHVACENO);

            modelAndView.addObject("rezervacije", rezervacije);
//            modelAndView.addObject("imaOdbijenih", imaOdbijenih);
            modelAndView.addObject("nemaNaCekanju", nemaNaCekanju);
            modelAndView.addObject("imaPrihvacenih", imaPrihvacenih);
        }

        return modelAndView;
    }

    @PostMapping("/create")
    public void create(
            @RequestParam(required = true) String nazivDestinacije,
            @RequestParam(required = true) String imageName, // Dobijamo naziv slike kao string
            @RequestParam(required = true) String datumVremePolaska,
            @RequestParam(required = true) String datumVremePovratka,
            @RequestParam(required = true) int brojNocenja,
            @RequestParam(required = true) double cena,
            @RequestParam(required = true) int ukupanBrojMesta,
            @RequestParam(required = true) int brojSlobodnihMesta,
            @RequestParam(required = true) SmestajnaJedinica smestajnaJedinica,
            @RequestParam(required = true) PrevoznoSredstvo prevoznoSredstvo,
            @RequestParam(required = true) Long kategorijaPutovanja,
            HttpServletResponse response) throws IOException {

        LocalDateTime parsedDatumVremePolaska = LocalDateTime.parse(datumVremePolaska);
        LocalDateTime parsedDatumVremePovratka = LocalDateTime.parse(datumVremePovratka);
        KategorijaPutovanja kategorija = kategorijaPutovanjaService.getKategorijaById(kategorijaPutovanja);
        imageName = "images/" + imageName;
        Putovanje putovanje = new Putovanje(nazivDestinacije, imageName, parsedDatumVremePolaska, parsedDatumVremePovratka,
                brojNocenja, cena, ukupanBrojMesta, brojSlobodnihMesta, smestajnaJedinica, prevoznoSredstvo, kategorija);

        putovanjeService.save(putovanje);

        response.sendRedirect("/travelagency/putovanja");
    }
    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Putovanje> update(@PathVariable Long id, @RequestBody Putovanje putovanje) {
        putovanje.setId(id);
        Putovanje updatedPutovanje = putovanjeService.save(putovanje);
        return new ResponseEntity<>(updatedPutovanje, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        putovanjeService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/kategorije", produces = "application/json")
    @ResponseBody
    public List<KategorijaPutovanja> getKategorije() {
        return kategorijaPutovanjaService.getAllKategorije();
    }


    @GetMapping("/dodavanjePutovanja")
    public ModelAndView getDodavanjePutovanjaPage(HttpSession session) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }
        ModelAndView modelAndView = new ModelAndView("dodavanjePutovanja");
        modelAndView.addObject("kategorijePutovanja", kategorijaPutovanjaService.getAllKategorije());
        modelAndView.addObject("smestajnaJedinicaOptions", SmestajnaJedinica.values());
        modelAndView.addObject("prevoznoSredstvoOptions", PrevoznoSredstvo.values());
        return modelAndView;
    }
    // Endpoint za izmjenu putovanja
    @GetMapping("/izmjeniPutovanje/{id}")
    public ModelAndView prikaziFormuZaIzmjenu(@PathVariable("id") Long id, HttpSession session) {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            return new ModelAndView("redirect:/korisnici/login");
        }
        ModelAndView modelAndView = new ModelAndView("izmjeniPutovanje");
        Putovanje putovanje = putovanjeService.getById(id);
        modelAndView.addObject("putovanje", putovanje);
        modelAndView.addObject("kategorijePutovanja", kategorijaPutovanjaService.getAllKategorije());
        modelAndView.addObject("smestajnaJedinicaOptions", SmestajnaJedinica.values());
        modelAndView.addObject("prevoznoSredstvoOptions", PrevoznoSredstvo.values());
        return modelAndView;
    }
    @PostMapping(value = "/sacuvajIzmjene", produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView updatePutovanje(
            @RequestParam Long id,
            @RequestParam String nazivDestinacije,
            @RequestParam String imageName,
            @RequestParam String datumVremePolaska,
            @RequestParam String datumVremePovratka,
            @RequestParam int brojNocenja,
            @RequestParam double cena,
            @RequestParam int ukupanBrojMesta,
            @RequestParam int brojSlobodnihMesta,
            @RequestParam SmestajnaJedinica smestajnaJedinica,
            @RequestParam PrevoznoSredstvo prevoznoSredstvo,
            @RequestParam Long kategorijaPutovanja) {

        try {
            LocalDateTime parsedDatumVremePolaska = LocalDateTime.parse(datumVremePolaska);
            LocalDateTime parsedDatumVremePovratka = LocalDateTime.parse(datumVremePovratka);
            KategorijaPutovanja kategorija = kategorijaPutovanjaService.getKategorijaById(kategorijaPutovanja);

            Putovanje existingPutovanje = putovanjeService.getById(id);

            if (existingPutovanje == null) {
                ModelAndView modelAndView = new ModelAndView("errorPage");
                modelAndView.addObject("errorMessage", "Putovanje nije pronađeno.");
                return modelAndView;
            }

            // Provera i dodavanje prefiksa za naziv slike
            if (!imageName.startsWith("images/")) {
                imageName = "images/" + imageName;
            }

            existingPutovanje.setNazivDestinacije(nazivDestinacije);
            existingPutovanje.setSlika(imageName);
            existingPutovanje.setDatumVremePolaska(parsedDatumVremePolaska);
            existingPutovanje.setDatumVremePovratka(parsedDatumVremePovratka);
            existingPutovanje.setBrojNocenja(brojNocenja);
            existingPutovanje.setCena(cena);
            existingPutovanje.setUkupanBrojMesta(ukupanBrojMesta);
            existingPutovanje.setBrojSlobodnihMesta(brojSlobodnihMesta);
            existingPutovanje.setSmestajnaJedinica(smestajnaJedinica);
            existingPutovanje.setPrevoznoSredstvo(prevoznoSredstvo);
            existingPutovanje.setKategorijaPutovanja(kategorija);

            putovanjeService.save(existingPutovanje);

            ModelAndView modelAndView = new ModelAndView("redirect:/putovanja");
            return modelAndView;

        } catch (Exception e) {
            ModelAndView modelAndView = new ModelAndView("errorPage");
            modelAndView.addObject("errorMessage", "Došlo je do greške prilikom ažuriranja putovanja.");
            return modelAndView;
        }
    }


    @PostMapping(value = "/obrisi")
    public void deletePutovanje(@RequestParam Long id,
                                HttpSession session, RedirectAttributes redirectAttributes, HttpServletResponse response) throws IOException {

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute("prijavljeniKorisnik");
        if (prijavljeniKorisnik == null) {
            response.sendRedirect("redirect:/korisnici/login");
        }

        boolean rezervacijePostoje = putovanjeService.hasReservations(id);
        if (rezervacijePostoje) {
            redirectAttributes.addFlashAttribute("errorMessage", "Ne možete obrisati putovanje jer postoje rezervacije za njega.");
        } else {
            putovanjeService.delete(id);
            redirectAttributes.addFlashAttribute("successMessage", "Putovanje uspešno obrisano!");
        }

        response.sendRedirect("/travelagency/putovanja");
    }
    @PostMapping("/dodajPopust")
    public ModelAndView dodajPopust(@RequestParam("putovanjeId") Long putovanjeId,
                                    @RequestParam("procenat") Double procenat,
                                    @RequestParam("pocetakVazenja") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime pocetakVazenja,
                                    @RequestParam("krajVazenja") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime krajVazenja,
                                    HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("putovanja");
        try {
            Putovanje putovanje = putovanjeService.getById(putovanjeId);
            if (putovanje != null) {
                Popust popust = new Popust();
                popust.setProcenat(procenat);
                popust.setPocetakVazenja(pocetakVazenja);
                popust.setKrajVazenja(krajVazenja);
                putovanje.setPopust(popust);
                putovanjeService.update(putovanje);
                response.sendRedirect("/travelagency/putovanja");
                modelAndView.addObject("successMessage", "Popust uspešno dodat.");
            } else {
                response.sendRedirect("/travelagency/putovanja");
                modelAndView.addObject("errorMessage", "Putovanje nije pronađeno.");
            }
        } catch (Exception e) {
            modelAndView.addObject("errorMessage", "Greška prilikom dodavanja popusta.");
        }
        return modelAndView;
    }
}
