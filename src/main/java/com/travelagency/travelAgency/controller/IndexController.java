package com.travelagency.travelAgency.controller;

import com.travelagency.travelAgency.model.KategorijaPutovanja;
import com.travelagency.travelAgency.model.Korisnik;
import com.travelagency.travelAgency.model.Putovanje;
import com.travelagency.travelAgency.model.Uloga;
import com.travelagency.travelAgency.service.KategorijaPutovanjaService;
import com.travelagency.travelAgency.service.PutovanjeService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@RequestMapping(value = "/")
public class IndexController {
    public static final String KORISNIK_KEY = "prijavljeniKorisnik";
    @Autowired
    private PutovanjeService putovanjeService;
    @Autowired
    private KategorijaPutovanjaService kategorijaPutovanjaService;
//    @GetMapping
//    public ModelAndView getPutovanja() {
//        ModelAndView modelAndView = new ModelAndView("index");
//        modelAndView.addObject("putovanja", putovanjeService.getAll());
//        return modelAndView;
//    }
    @GetMapping
    public ModelAndView getPutovanja(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("index");
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);

        modelAndView.addObject("kategorije", kategorijaPutovanjaService.getAllKategorije());

        if (prijavljeniKorisnik != null && prijavljeniKorisnik.getUloga() == Uloga.MENAGER) {
            modelAndView.addObject("putovanja", putovanjeService.getAll());
        } else {
            modelAndView.addObject("putovanja", putovanjeService.getPutovanjaSaSlobodnimMjestima());
        }
        return modelAndView;
    }

    @GetMapping("/pretraga")
    public ModelAndView search(
            HttpSession session,
            @RequestParam(value = "prevoznoSredstvo", required = false) String prevoznoSredstvo,
            @RequestParam(value = "nazivDestinacije", required = false) String nazivDestinacije,
            @RequestParam(value = "kategorijaPutovanja", required = false) Long kategorijaId,
            @RequestParam(value = "brojNocenja", required = false) Integer brojNocenja,
            @RequestParam(value = "minCena", required = false) Double minCena,
            @RequestParam(value = "maxCena", required = false) Double maxCena) {

        ModelAndView mav = new ModelAndView("index");
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);

        mav.addObject("kategorije", kategorijaPutovanjaService.getAllKategorije());

        if (prijavljeniKorisnik != null && prijavljeniKorisnik.getUloga() == Uloga.MENAGER) {
            List<Putovanje> putovanja = putovanjeService.searchPutovanja(prevoznoSredstvo, nazivDestinacije, kategorijaId, brojNocenja, minCena, maxCena);
            mav.addObject("putovanja", putovanja);
        } else {
            List<Putovanje> putovanja = putovanjeService.searchPutovanjaSaSlobodnimMjestima(prevoznoSredstvo, nazivDestinacije, kategorijaId, brojNocenja, minCena, maxCena);
            mav.addObject("putovanja", putovanja);
        }

        return mav;
    }


    @GetMapping("/sortiraj")
    public ModelAndView sort(
            HttpSession session,
            @RequestParam(value = "sortirajPo", required = false) String sortBy) {

        ModelAndView mav = new ModelAndView("index");
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);

        if (prijavljeniKorisnik != null && prijavljeniKorisnik.getUloga() == Uloga.MENAGER) {
            // Menadžer može videti sva putovanja
            List<Putovanje> putovanja = putovanjeService.sortPutovanja(sortBy);
            mav.addObject("putovanja", putovanja);
        } else {
            // Kupci mogu videti samo putovanja sa slobodnim mestima
            List<Putovanje> putovanja = putovanjeService.sortPutovanjaSaSlobodnimMjestima(sortBy);
            mav.addObject("putovanja", putovanja);
        }

        return mav;
    }


}