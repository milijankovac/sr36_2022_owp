package com.travelagency.travelAgency.dao;

import com.travelagency.travelAgency.model.Rezervacija;

import java.util.List;

public interface RezervacijaDAO {
    public Rezervacija findOne(Long id);
    public List<Rezervacija> findAll();
    public int save(Rezervacija rezervacija);
    public int update(Rezervacija rezervacija);
    public int delete(Rezervacija rezervacija);
    public List<Rezervacija> findFinished();
    public List<Rezervacija> findActive();
    List<Rezervacija> findByKorisnikAndPutovanje(Long korisnikId, Long putovanjeId);
    List<Rezervacija> findByKorisnik(Long korisnikId);
    boolean existsByPutovanjeId(Long putovanjeId);
    int prihvatiRezervaciju(Long rezervacijaId);
    int odbijRezervaciju(Long rezervacijaId);
}
