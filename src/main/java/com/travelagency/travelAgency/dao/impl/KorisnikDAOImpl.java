package com.travelagency.travelAgency.dao.impl;


import com.travelagency.travelAgency.dao.KorisnikDAO;
import com.travelagency.travelAgency.model.Korisnik;
import com.travelagency.travelAgency.model.Uloga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class KorisnikRowCallBackHandler implements RowCallbackHandler {

        private Map<Long, Korisnik> korisnici = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            Long id = resultSet.getLong("id");
            String ime = resultSet.getString("ime");
            String prezime = resultSet.getString("prezime");
            String email = resultSet.getString("email");
            String lozinka = resultSet.getString("lozinka");
            LocalDate datumRodjenja = resultSet.getDate("datum_rodjenja") != null ? resultSet.getDate("datum_rodjenja").toLocalDate() : null;
            String jmbg = resultSet.getString("jmbg");
            String adresa = resultSet.getString("adresa");
            Integer brojTelefona = resultSet.getInt("broj_telefona");
            LocalDateTime datumVremeRegistracije = resultSet.getTimestamp("datum_vreme_registracije") != null ? resultSet.getTimestamp("datum_vreme_registracije").toLocalDateTime() : null;
            String ulogaString = resultSet.getString("uloga");

            Uloga uloga = Uloga.valueOf(ulogaString);

            Korisnik korisnik = korisnici.get(id);
            if (korisnik == null) {
                korisnik = new Korisnik();
                korisnik.setId(id);
                korisnik.setIme(ime);
                korisnik.setPrezime(prezime);
                korisnik.setEmail(email);
                korisnik.setLozinka(lozinka);
                korisnik.setDatumRodjenja(datumRodjenja);
                korisnik.setJmbg(jmbg);
                korisnik.setAdresa(adresa);
                korisnik.setBrojTelefona(brojTelefona);
                korisnik.setDatumVremeRegistracije(datumVremeRegistracije);
                korisnik.setUloga(uloga);

                korisnici.put(id, korisnik); // Dodavanje u kolekciju
            }
        }


        public List<Korisnik> getKorisnici() {
            return new ArrayList<>(korisnici.values());
        }
    }

    @Override
    public Korisnik findById(Long id) {
        String sql =
                "SELECT kor.id, kor.ime, kor.prezime, kor.email, kor.lozinka, kor.datum_rodjenja, kor.jmbg, kor.adresa, kor.broj_telefona, kor.datum_vreme_registracije, kor.uloga " +
                        "FROM korisnik kor " +
                        "WHERE kor.id = ? " +
                        "ORDER BY kor.id";

        KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);

        return rowCallbackHandler.getKorisnici().get(0);
    }

    @Override
    public Korisnik findByEmail(String email) {
        String sql =
                "SELECT kor.id, kor.ime, kor.prezime, kor.email, kor.lozinka, kor.datum_rodjenja, kor.jmbg, kor.adresa, kor.broj_telefona, kor.datum_vreme_registracije, kor.uloga " +
                        "FROM korisnik kor " +
                        "WHERE kor.email = ? " +
                        "ORDER BY kor.id";

        KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, email);

        return rowCallbackHandler.getKorisnici().get(0);
    }

    @Override
    public Korisnik findForLogin(String email, String sifra) {
        String sql =
                "SELECT kor.id, kor.ime, kor.prezime, kor.email, kor.lozinka, kor.datum_rodjenja, kor.jmbg, kor.adresa, kor.broj_telefona, kor.datum_vreme_registracije, kor.uloga " +
                        "FROM korisnik kor " +
                        "WHERE kor.email = ? AND " +
                        "kor.lozinka = ? " +
                        "ORDER BY kor.id";

        KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, email, sifra);

        if(rowCallbackHandler.getKorisnici().size() == 0) {
            return null;
        }

        return rowCallbackHandler.getKorisnici().get(0);
    }

    @Override
    public List<Korisnik> findAll() {
        String sql =
                "SELECT kor.id, kor.ime, kor.prezime, kor.email, kor.lozinka, kor.datum_rodjenja, kor.jmbg, kor.adresa, kor.broj_telefona, kor.datum_vreme_registracije, kor.uloga " +
                        "FROM korisnik kor " +
                        "ORDER BY kor.id";

        KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getKorisnici();
    }

    @org.springframework.transaction.annotation.Transactional
    @Override
    public int save(Korisnik korisnik) {
        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                String sql = "INSERT INTO korisnik (ime, prezime, email, lozinka, datum_rodjenja, jmbg, adresa, broj_telefona, datum_vreme_registracije, uloga) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setString(index++, korisnik.getIme());
                preparedStatement.setString(index++, korisnik.getPrezime());
                preparedStatement.setString(index++, korisnik.getEmail());
                preparedStatement.setString(index++, korisnik.getLozinka());
                preparedStatement.setDate(index++, korisnik.getDatumRodjenja() != null ? Date.valueOf(korisnik.getDatumRodjenja()) : null);
                preparedStatement.setString(index++, korisnik.getJmbg());
                preparedStatement.setString(index++, korisnik.getAdresa());
                preparedStatement.setInt(index++, korisnik.getBrojTelefona());
                preparedStatement.setTimestamp(index++, korisnik.getDatumVremeRegistracije() != null ? Timestamp.valueOf(korisnik.getDatumVremeRegistracije()) : null);
                preparedStatement.setString(index++, korisnik.getUloga().name());

                return preparedStatement;
            }

        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return uspeh ? 1 : 0;
    }

    @Transactional
    @Override
    public int update(Korisnik korisnik) {
        String sql = "UPDATE korisnik SET ime = ?, prezime = ?, email = ?, lozinka = ?, datum_rodjenja = ?, jmbg = ?, adresa = ?, broj_telefona = ?, datum_vreme_registracije = ?, uloga = ? WHERE id = ?";
        boolean uspeh = jdbcTemplate.update(sql,
                korisnik.getIme(),
                korisnik.getPrezime(),
                korisnik.getEmail(),
                korisnik.getLozinka(),
                korisnik.getDatumRodjenja() != null ? Date.valueOf(korisnik.getDatumRodjenja()) : null,
                korisnik.getJmbg(),
                korisnik.getAdresa(),
                korisnik.getBrojTelefona(),
                korisnik.getDatumVremeRegistracije() != null ? Timestamp.valueOf(korisnik.getDatumVremeRegistracije()) : null,
                korisnik.getUloga().name(),
                korisnik.getId()) == 1;

        return uspeh ? 1 : 0;
    }

    @Transactional
    @Override
    public int delete(Long id) {
        String sql = "DELETE FROM korisnik WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
    @Override
    public boolean updatePassword(Korisnik korisnik, String newPassword) {
        try {
            // Ažuriranje samo lozinke korisnika
            String sql = "UPDATE korisnik SET lozinka = ? WHERE id = ?";
            jdbcTemplate.update(sql, newPassword, korisnik.getId());
            System.out.println("Lozinka uspešno promenjena za korisnika: " + korisnik.getEmail());
            return true;
        } catch (Exception e) {
            System.out.println("Greška prilikom promene lozinke za korisnika: " + korisnik.getEmail());
            e.printStackTrace();
            return false;
        }
    }
}