package com.travelagency.travelAgency.dao.impl;


import com.travelagency.travelAgency.dao.KategorijaPutovanjaDAO;
import com.travelagency.travelAgency.model.KategorijaPutovanja;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class KategorijaPutovanjaDAOImpl implements KategorijaPutovanjaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final class KategorijaPutovanjaMapper implements RowMapper<KategorijaPutovanja> {


        @Override
        public KategorijaPutovanja mapRow(ResultSet rs, int rowNum) throws SQLException {
            KategorijaPutovanja kategorijaPutovanja = new KategorijaPutovanja();
            kategorijaPutovanja.setId(rs.getLong("id"));
            kategorijaPutovanja.setNaziv(rs.getString("naziv"));
            kategorijaPutovanja.setOpis(rs.getString("opis"));
            return kategorijaPutovanja;
        }
    }

    public KategorijaPutovanja findOne(Long id) {
        String sql = "SELECT id, naziv, opis FROM kategorija_putovanja WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<KategorijaPutovanja>() {
            @Override
            public KategorijaPutovanja mapRow(ResultSet rs, int rowNum) throws SQLException {
                KategorijaPutovanja kategorija = new KategorijaPutovanja();
                kategorija.setId(rs.getLong("id"));
                kategorija.setNaziv(rs.getString("naziv"));
                kategorija.setOpis(rs.getString("opis"));
                return kategorija;
            }
        });
    }

    @Override
    public List<KategorijaPutovanja> findAll() {
        String sql = "SELECT * FROM kategorija_putovanja";
        return jdbcTemplate.query(sql, new KategorijaPutovanjaMapper());
    }

    @Override
    public int save(KategorijaPutovanja kategorijaPutovanja) {
        if (kategorijaPutovanja.getId() == null) {
            String sql = "INSERT INTO kategorija_putovanja (naziv, opis) VALUES (?, ?)";
            jdbcTemplate.update(sql, kategorijaPutovanja.getNaziv(), kategorijaPutovanja.getOpis());
            Long newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Long.class);
            kategorijaPutovanja.setId(newId);
            return 1; // Vrati 1 kao indikator uspešnog unosa
        } else {
            return update(kategorijaPutovanja); // Ako postoji ID, pozivamo metodu za ažuriranje
        }
    }

    @Override
    public int update(KategorijaPutovanja kategorijaPutovanja) {
        String sql = "UPDATE kategorija_putovanja SET naziv = ?, opis = ? WHERE id = ?";
        return jdbcTemplate.update(sql, kategorijaPutovanja.getNaziv(), kategorijaPutovanja.getOpis(), kategorijaPutovanja.getId());
    }

    @Override
    public int delete(KategorijaPutovanja kategorijaPutovanja) {
        String sql = "DELETE FROM kategorija_putovanja WHERE id = ?";
        return jdbcTemplate.update(sql, kategorijaPutovanja.getId());
    }
}