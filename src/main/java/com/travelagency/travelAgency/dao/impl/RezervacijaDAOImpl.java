package com.travelagency.travelAgency.dao.impl;

import com.travelagency.travelAgency.dao.RezervacijaDAO;
import com.travelagency.travelAgency.model.Korisnik;
import com.travelagency.travelAgency.model.Putovanje;
import com.travelagency.travelAgency.model.Rezervacija;
import com.travelagency.travelAgency.model.StatusRezervacije;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RezervacijaDAOImpl implements RezervacijaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class RezervacijaRowCallBackHandler implements RowCallbackHandler {

        private Map<Long, Rezervacija> rezervacije = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            Long id = resultSet.getLong("id");
            Long korisnikId = resultSet.getLong("korisnik_id");
            Long putovanjeId = resultSet.getLong("putovanje_id");
            Integer brojMesta = resultSet.getInt("broj_mesta");
            Double ukupnaCena = resultSet.getDouble("ukupna_cena");
            String status = resultSet.getString("status");

            Rezervacija rezervacija = rezervacije.get(id);
            if (rezervacija == null) {
                rezervacija = new Rezervacija();
                rezervacija.setId(id);
                Korisnik korisnik = new Korisnik();
                korisnik.setId(korisnikId);
                rezervacija.setKorisnik(korisnik);
                Putovanje putovanje = new Putovanje();
                putovanje.setId(putovanjeId);
                rezervacija.setPutovanje(putovanje);
                rezervacija.setBrojMesta(brojMesta);
                rezervacija.setUkupnaCena(ukupnaCena);
                rezervacija.setStatusRezervacije(StatusRezervacije.valueOf(status));

                rezervacije.put(id, rezervacija);
            }
        }

        public List<Rezervacija> getRezervacije() {
            return new ArrayList<>(rezervacije.values());
        }
    }

    @Override
    public Rezervacija findOne(Long id) {
        String sql = "SELECT * FROM rezervacije WHERE id = ?";

        RezervacijaRowCallBackHandler rowCallbackHandler = new RezervacijaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);

        return rowCallbackHandler.getRezervacije().isEmpty() ? null : rowCallbackHandler.getRezervacije().get(0);
    }

    @Override
    public List<Rezervacija> findAll() {
        String sql = "SELECT * FROM rezervacije";

        RezervacijaRowCallBackHandler rowCallbackHandler = new RezervacijaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getRezervacije();
    }

    @Transactional
    @Override
    public int save(Rezervacija rezervacija) {
        PreparedStatementCreator preparedStatementCreator = connection -> {
            String sql = "INSERT INTO rezervacije (korisnik_id, putovanje_id, broj_mesta, ukupna_cena, status) " +
                    "VALUES (?, ?, ?, ?, ?)";

            PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            int index = 1;
            preparedStatement.setLong(index++, rezervacija.getKorisnik().getId());
            preparedStatement.setLong(index++, rezervacija.getPutovanje().getId());
            preparedStatement.setInt(index++, rezervacija.getBrojMesta());
            preparedStatement.setDouble(index++, rezervacija.getUkupnaCena());
            preparedStatement.setString(index++, rezervacija.getStatusRezervacije().name());

            return preparedStatement;
        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int update(Rezervacija rezervacija) {
        String sql = "UPDATE rezervacije SET korisnik_id = ?, putovanje_id = ?, broj_mesta = ?, " +
                "ukupna_cena = ?, status = ? WHERE id = ?";
        boolean success = jdbcTemplate.update(sql,
                rezervacija.getKorisnik().getId(),
                rezervacija.getPutovanje().getId(),
                rezervacija.getBrojMesta(),
                rezervacija.getUkupnaCena(),
                rezervacija.getStatusRezervacije().name(),
                rezervacija.getId()) == 1;

        return success ? 1 : 0;
    }

    @Transactional
    @Override
    public int delete(Rezervacija rezervacija) {
        String sql = "DELETE FROM rezervacije WHERE id = ?";
        return jdbcTemplate.update(sql, rezervacija.getId());
    }

    @Override
    public List<Rezervacija> findFinished() {
        String sql = "SELECT * FROM rezervacije r JOIN putovanje p ON r.putovanje_id = p.id WHERE p.datum_vreme_polaska < NOW()";

        RezervacijaRowCallBackHandler rowCallbackHandler = new RezervacijaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getRezervacije();
    }

    @Override
    public List<Rezervacija> findActive() {
        String sql = "SELECT * FROM rezervacije r JOIN putovanje p ON r.putovanje_id = p.id WHERE p.datum_vreme_polaska > NOW()";

        RezervacijaRowCallBackHandler rowCallbackHandler = new RezervacijaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);

        return rowCallbackHandler.getRezervacije();
    }

    @Override
    public List<Rezervacija> findByKorisnikAndPutovanje(Long korisnikId, Long putovanjeId) {
        String sql = "SELECT * FROM rezervacije WHERE korisnik_id = ? AND putovanje_id = ?";
        RezervacijaRowCallBackHandler rowCallbackHandler = new RezervacijaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, korisnikId, putovanjeId);
        return rowCallbackHandler.getRezervacije();
    }

    @Override
    public List<Rezervacija> findByKorisnik(Long korisnikId) {
        String sql = "SELECT * FROM rezervacije WHERE korisnik_id = ?";

        RezervacijaRowCallBackHandler rowCallbackHandler = new RezervacijaRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, korisnikId);

        return rowCallbackHandler.getRezervacije();
    }

    @Override
    public boolean existsByPutovanjeId(Long putovanjeId) {
        String sql = "SELECT COUNT(*) FROM rezervacije WHERE putovanje_id = ?";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class, putovanjeId);
        return count != null && count > 0;
    }
    @Transactional
    @Override
    public int prihvatiRezervaciju(Long rezervacijaId) {
        String sql = "UPDATE rezervacije SET status = ? WHERE id = ?";
        return jdbcTemplate.update(sql, StatusRezervacije.PRIHVACENO.name(), rezervacijaId);
    }

    @Transactional
    @Override
    public int odbijRezervaciju(Long rezervacijaId) {
        String sql = "UPDATE rezervacije SET status = ? WHERE id = ?";
        return jdbcTemplate.update(sql, StatusRezervacije.ODBIJENO.name(), rezervacijaId);
    }
}
