package com.travelagency.travelAgency.dao.impl;

import com.travelagency.travelAgency.dao.KategorijaPutovanjaDAO;
import com.travelagency.travelAgency.dao.PutovanjeDAO;
import com.travelagency.travelAgency.model.KategorijaPutovanja;
import com.travelagency.travelAgency.model.PrevoznoSredstvo;
import com.travelagency.travelAgency.model.Putovanje;
import com.travelagency.travelAgency.model.SmestajnaJedinica;
import com.travelagency.travelAgency.model.Popust;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class PutovanjeDAOImpl implements PutovanjeDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KategorijaPutovanjaDAO kategorijaPutovanjaDAO;

    private static final class PutovanjeRowMapper implements RowMapper<Putovanje> {

        private final KategorijaPutovanjaDAO kategorijaPutovanjaDAO;

        public PutovanjeRowMapper(KategorijaPutovanjaDAO kategorijaPutovanjaDAO) {
            this.kategorijaPutovanjaDAO = kategorijaPutovanjaDAO;
        }

        @Override
        public Putovanje mapRow(ResultSet rs, int rowNum) throws SQLException {
            Putovanje putovanje = new Putovanje();
            putovanje.setId(rs.getLong("id"));
            putovanje.setNazivDestinacije(rs.getString("naziv_destinacije"));
            putovanje.setSlika(rs.getString("slika"));

            Timestamp datumVremePolaska = rs.getTimestamp("datum_vreme_polaska");
            putovanje.setDatumVremePolaska(datumVremePolaska != null ? datumVremePolaska.toLocalDateTime() : null);

            Timestamp datumVremePovratka = rs.getTimestamp("datum_vreme_povratka");
            putovanje.setDatumVremePovratka(datumVremePovratka != null ? datumVremePovratka.toLocalDateTime() : null);

            putovanje.setBrojNocenja(rs.getInt("broj_nocenja"));
            putovanje.setCena(rs.getDouble("cena"));
            putovanje.setUkupanBrojMesta(rs.getInt("ukupan_broj_mesta"));
            putovanje.setBrojSlobodnihMesta(rs.getInt("broj_slobodnih_mesta"));
            putovanje.setSmestajnaJedinica(SmestajnaJedinica.valueOf(rs.getString("smestajna_jedinica")));
            putovanje.setPrevoznoSredstvo(PrevoznoSredstvo.valueOf(rs.getString("prevozno_sredstvo")));

            Long kategorijaPutovanjaId = rs.getLong("kategorija_id");
            KategorijaPutovanja kategorijaPutovanja = kategorijaPutovanjaDAO.findOne(kategorijaPutovanjaId);
            putovanje.setKategorijaPutovanja(kategorijaPutovanja);

            // Mapiranje polja za popust
            Double procenat = rs.getDouble("procenat");
            if (procenat != null) {
                Timestamp pocetakVazenja = rs.getTimestamp("pocetak_vazenja");
                Timestamp krajVazenja = rs.getTimestamp("kraj_vazenja");

                Popust popust = new Popust(
                        procenat,
                        pocetakVazenja != null ? pocetakVazenja.toLocalDateTime() : null,
                        krajVazenja != null ? krajVazenja.toLocalDateTime() : null
                );
                putovanje.setPopust(popust);
            }

            return putovanje;
        }

    }

    @Override
    public Putovanje findById(Long id) {
        String sql = "SELECT * FROM putovanje WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public List<Putovanje> findAll() {
        String sql = "SELECT * FROM putovanje ORDER BY id";
        return jdbcTemplate.query(sql, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public Putovanje save(Putovanje putovanje) {
        if (putovanje.getId() == null) {
            String sql = "INSERT INTO putovanje (naziv_destinacije, slika, datum_vreme_polaska, datum_vreme_povratka, broj_nocenja, cena, ukupan_broj_mesta, broj_slobodnih_mesta, smestajna_jedinica, prevozno_sredstvo, kategorija_id, procenat, pocetak_vazenja, kraj_vazenja) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, putovanje.getNazivDestinacije(), putovanje.getSlika(), putovanje.getDatumVremePolaska(), putovanje.getDatumVremePovratka(), putovanje.getBrojNocenja(), putovanje.getCena(), putovanje.getUkupanBrojMesta(), putovanje.getBrojSlobodnihMesta(), putovanje.getSmestajnaJedinica().name(), putovanje.getPrevoznoSredstvo().name(), putovanje.getKategorijaPutovanja().getId(), putovanje.getPopust() != null ? putovanje.getPopust().getProcenat() : null, putovanje.getPopust() != null ? putovanje.getPopust().getPocetakVazenja() : null, putovanje.getPopust() != null ? putovanje.getPopust().getKrajVazenja() : null);
            Long newId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Long.class);
            putovanje.setId(newId);
        } else {
            String sql = "UPDATE putovanje SET naziv_destinacije = ?, slika = ?, datum_vreme_polaska = ?, datum_vreme_povratka = ?, broj_nocenja = ?, cena = ?, ukupan_broj_mesta = ?, broj_slobodnih_mesta = ?, smestajna_jedinica = ?, prevozno_sredstvo = ?, kategorija_id = ?, procenat = ?, pocetak_vazenja = ?, kraj_vazenja = ? WHERE id = ?";
            jdbcTemplate.update(sql, putovanje.getNazivDestinacije(), putovanje.getSlika(), putovanje.getDatumVremePolaska(), putovanje.getDatumVremePovratka(), putovanje.getBrojNocenja(), putovanje.getCena(), putovanje.getUkupanBrojMesta(), putovanje.getBrojSlobodnihMesta(), putovanje.getSmestajnaJedinica().name(), putovanje.getPrevoznoSredstvo().name(), putovanje.getKategorijaPutovanja().getId(), putovanje.getPopust() != null ? putovanje.getPopust().getProcenat() : null, putovanje.getPopust() != null ? putovanje.getPopust().getPocetakVazenja() : null, putovanje.getPopust() != null ? putovanje.getPopust().getKrajVazenja() : null, putovanje.getId());
        }
        return putovanje;
    }

    @Override
    @Transactional
    public Putovanje update(Putovanje putovanje) {
        String sql = "UPDATE putovanje SET naziv_destinacije = ?, slika = ?, datum_vreme_polaska = ?, datum_vreme_povratka = ?, " +
                "broj_nocenja = ?, cena = ?, ukupan_broj_mesta = ?, broj_slobodnih_mesta = ?, smestajna_jedinica = ?, " +
                "prevozno_sredstvo = ?, kategorija_id = ?, procenat = ?, pocetak_vazenja = ?, kraj_vazenja = ? WHERE id = ?";
        jdbcTemplate.update(sql, putovanje.getNazivDestinacije(), putovanje.getSlika(), putovanje.getDatumVremePolaska(),
                putovanje.getDatumVremePovratka(), putovanje.getBrojNocenja(), putovanje.getCena(),
                putovanje.getUkupanBrojMesta(), putovanje.getBrojSlobodnihMesta(),
                putovanje.getSmestajnaJedinica().name(), putovanje.getPrevoznoSredstvo().name(),
                putovanje.getKategorijaPutovanja().getId(), putovanje.getPopust() != null ? putovanje.getPopust().getProcenat() : null, putovanje.getPopust() != null ? putovanje.getPopust().getPocetakVazenja() : null, putovanje.getPopust() != null ? putovanje.getPopust().getKrajVazenja() : null, putovanje.getId());
        return putovanje;
    }

    @Override
    public int delete(Long id) {
        String sql = "DELETE FROM putovanje WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }

    @Override
    public List<Putovanje> findByCenaBetween(double minCena, double maxCena) {
        String sql = "SELECT * FROM putovanje WHERE cena BETWEEN ? AND ?";
        return jdbcTemplate.query(sql, new Object[]{minCena, maxCena}, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public List<Putovanje> findByCenaGreaterThanEqual(double minCena) {
        String sql = "SELECT * FROM putovanje WHERE cena >= ?";
        return jdbcTemplate.query(sql, new Object[]{minCena}, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public List<Putovanje> findByCenaLessThanEqual(double maxCena) {
        String sql = "SELECT * FROM putovanje WHERE cena <= ?";
        return jdbcTemplate.query(sql, new Object[]{maxCena}, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public List<Putovanje> findByBrojSlobodnihMestaGreaterThan(int brojSlobodnihMesta) {
        String sql = "SELECT * FROM putovanje WHERE broj_slobodnih_mesta > ?";
        return jdbcTemplate.query(sql, new Object[]{brojSlobodnihMesta}, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public List<Putovanje> search(String prevoznoSredstvo, String nazivDestinacije, Long kategorijaId, Integer brojNocenja, Double minCena, Double maxCena) {
        String sql = "SELECT * FROM putovanje WHERE 1=1";
        if (prevoznoSredstvo != null && !prevoznoSredstvo.isEmpty()) {
            sql += " AND prevozno_sredstvo = '" + prevoznoSredstvo + "'";
        }
        if (nazivDestinacije != null && !nazivDestinacije.isEmpty()) {
            sql += " AND naziv_destinacije LIKE '%" + nazivDestinacije + "%'";
        }
        if (kategorijaId != null) {
            sql += " AND kategorija_id = " + kategorijaId;
        }
        if (brojNocenja != null) {
            sql += " AND broj_nocenja = " + brojNocenja;
        }
        if (minCena != null) {
            sql += " AND cena >= " + minCena;
        }
        if (maxCena != null) {
            sql += " AND cena <= " + maxCena;
        }
        return jdbcTemplate.query(sql, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }

    @Override
    public List<Putovanje> sortBy(String sortBy) {
        if ("nazivDestinacije".equals(sortBy)) {
            sortBy = "naziv_destinacije"; // Zamenite sa ispravnim nazivom kolone
        } else if ("brojNocenja".equals(sortBy)) {
            sortBy = "broj_nocenja";
        }
        String sql = "SELECT * FROM putovanje ORDER BY " + sortBy;
        return jdbcTemplate.query(sql, new PutovanjeRowMapper(kategorijaPutovanjaDAO));
    }
}
