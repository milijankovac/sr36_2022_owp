package com.travelagency.travelAgency.dao;

import com.travelagency.travelAgency.model.KategorijaPutovanja;

import java.util.List;

public interface KategorijaPutovanjaDAO {
    public KategorijaPutovanja findOne(Long id);
    public List<KategorijaPutovanja> findAll();
    public int save(KategorijaPutovanja kategorijaPutovanja);

    public int update(KategorijaPutovanja kategorijaPutovanja);
    public int delete(KategorijaPutovanja kategorijaPutovanja);
}
