package com.travelagency.travelAgency.dao;

import com.travelagency.travelAgency.model.Putovanje;

import java.util.List;

public interface PutovanjeDAO {
    public Putovanje save(Putovanje putovanje);
    Putovanje update(Putovanje putovanje);
    public Putovanje findById(Long id);
    List<Putovanje> findAll();
    public int delete(Long id);
    List<Putovanje> findByCenaBetween(double minCena, double maxCena);
    List<Putovanje> findByCenaGreaterThanEqual(double minCena);
    List<Putovanje> findByCenaLessThanEqual(double maxCena);
    List<Putovanje> findByBrojSlobodnihMestaGreaterThan(int brojSlobodnihMesta);
    List<Putovanje> search(String prevoznoSredstvo, String nazivDestinacije, Long kategorijaId, Integer brojNocenja, Double minCena, Double maxCena);
    List<Putovanje> sortBy(String sortBy);

}