package com.travelagency.travelAgency.dao;


import com.travelagency.travelAgency.model.Korisnik;

import java.util.List;

public interface KorisnikDAO {
    public Korisnik findById(Long id);

    public Korisnik findByEmail(String email);

    public Korisnik findForLogin(String email, String lozinka);

    public List<Korisnik> findAll();

    public int save(Korisnik korisnik);

    public int update(Korisnik korisnik);

    public int delete(Long id);
    boolean updatePassword(Korisnik korisnik, String newPassword);
}