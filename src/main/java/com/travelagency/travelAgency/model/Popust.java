package com.travelagency.travelAgency.model;

import jakarta.persistence.Embeddable;
import java.time.LocalDateTime;

@Embeddable
public class Popust {
    private Double procenat;
    private LocalDateTime pocetakVazenja;
    private LocalDateTime krajVazenja;

    public Popust() {
    }

    public Popust(Double procenat, LocalDateTime pocetakVazenja, LocalDateTime krajVazenja) {
        this.procenat = procenat;
        this.pocetakVazenja = pocetakVazenja;
        this.krajVazenja = krajVazenja;
    }

    public Double getProcenat() {
        return procenat;
    }

    public void setProcenat(Double procenat) {
        this.procenat = procenat;
    }

    public LocalDateTime getPocetakVazenja() {
        return pocetakVazenja;
    }

    public void setPocetakVazenja(LocalDateTime pocetakVazenja) {
        this.pocetakVazenja = pocetakVazenja;
    }

    public LocalDateTime getKrajVazenja() {
        return krajVazenja;
    }

    public void setKrajVazenja(LocalDateTime krajVazenja) {
        this.krajVazenja = krajVazenja;
    }

    public boolean isValid() {
        LocalDateTime now = LocalDateTime.now();
        return now.isAfter(pocetakVazenja) && now.isBefore(krajVazenja);
    }
}
