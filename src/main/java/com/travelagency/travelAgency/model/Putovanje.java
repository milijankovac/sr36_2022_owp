package com.travelagency.travelAgency.model;

import java.time.LocalDateTime;

import jakarta.persistence.*;

@Entity
@Table(name = "putovanje")
public class Putovanje {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "naziv_destinacije")
    private String nazivDestinacije;

    private String slika;

    @Column(name = "datum_vreme_polaska")
    private LocalDateTime datumVremePolaska;

    @Column(name = "datum_vreme_povratka")
    private LocalDateTime datumVremePovratka;

    @Column(name = "broj_nocenja")
    private Integer brojNocenja;

    private Double cena;

    @Column(name = "ukupan_broj_mesta")
    private Integer ukupanBrojMesta;

    @Column(name = "broj_slobodnih_mesta")
    private Integer brojSlobodnihMesta;

    @Enumerated(EnumType.STRING)
    private SmestajnaJedinica smestajnaJedinica;

    @Enumerated(EnumType.STRING)
    private PrevoznoSredstvo prevoznoSredstvo;

    @OneToOne
    @JoinColumn(name = "kategorija_id")
    private KategorijaPutovanja kategorijaPutovanja;

    private Popust popust;

    public Putovanje() {
    }

    public Putovanje(String nazivDestinacije, String slika, LocalDateTime datumVremePolaska, LocalDateTime datumVremePovratka, Integer brojNocenja, Double cena, Integer ukupanBrojMesta, Integer brojSlobodnihMesta, SmestajnaJedinica smestajnaJedinica, PrevoznoSredstvo prevoznoSredstvo, KategorijaPutovanja kategorijaPutovanja) {
        this.nazivDestinacije = nazivDestinacije;
        this.slika = slika;
        this.datumVremePolaska = datumVremePolaska;
        this.datumVremePovratka = datumVremePovratka;
        this.brojNocenja = brojNocenja;
        this.cena = cena;
        this.ukupanBrojMesta = ukupanBrojMesta;
        this.brojSlobodnihMesta = brojSlobodnihMesta;
        this.smestajnaJedinica = smestajnaJedinica;
        this.prevoznoSredstvo = prevoznoSredstvo;
        this.kategorijaPutovanja = kategorijaPutovanja;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazivDestinacije() {
        return nazivDestinacije;
    }

    public void setNazivDestinacije(String nazivDestinacije) {
        this.nazivDestinacije = nazivDestinacije;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public LocalDateTime getDatumVremePolaska() {
        return datumVremePolaska;
    }

    public void setDatumVremePolaska(LocalDateTime datumVremePolaska) {
        this.datumVremePolaska = datumVremePolaska;
    }

    public LocalDateTime getDatumVremePovratka() {
        return datumVremePovratka;
    }

    public void setDatumVremePovratka(LocalDateTime datumVremePovratka) {
        this.datumVremePovratka = datumVremePovratka;
    }

    public Integer getBrojNocenja() {
        return brojNocenja;
    }

    public void setBrojNocenja(Integer brojNocenja) {
        this.brojNocenja = brojNocenja;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public Integer getUkupanBrojMesta() {
        return ukupanBrojMesta;
    }

    public void setUkupanBrojMesta(Integer ukupanBrojMesta) {
        this.ukupanBrojMesta = ukupanBrojMesta;
    }

    public Integer getBrojSlobodnihMesta() {
        return brojSlobodnihMesta;
    }

    public void setBrojSlobodnihMesta(Integer brojSlobodnihMesta) {
        this.brojSlobodnihMesta = brojSlobodnihMesta;
    }

    public SmestajnaJedinica getSmestajnaJedinica() {
        return smestajnaJedinica;
    }

    public void setSmestajnaJedinica(SmestajnaJedinica smestajnaJedinica) {
        this.smestajnaJedinica = smestajnaJedinica;
    }

    public PrevoznoSredstvo getPrevoznoSredstvo() {
        return prevoznoSredstvo;
    }

    public void setPrevoznoSredstvo(PrevoznoSredstvo prevoznoSredstvo) {
        this.prevoznoSredstvo = prevoznoSredstvo;
    }

    public KategorijaPutovanja getKategorijaPutovanja() {
        return kategorijaPutovanja;
    }

    public void setKategorijaPutovanja(KategorijaPutovanja kategorijaPutovanja) {
        this.kategorijaPutovanja = kategorijaPutovanja;
    }
    public Popust getPopust() {
        return popust;
    }

    public void setPopust(Popust popust) {
        this.popust = popust;
    }
    public Double getCenaSaPopustom() {
        if (popust != null) {
            double popustDecimal = popust.getProcenat() / 100.0;
            double cenaSaPopustom = cena * (1 - popustDecimal);
            return Math.round(cenaSaPopustom * 100.0) / 100.0; // Zaokruživanje na dve decimale
        }
        return cena; // Ako nema popusta, vraćamo osnovnu cenu
    }

    public boolean isReserved() {
        return this.getBrojSlobodnihMesta() != this.getUkupanBrojMesta();
    }

    public boolean jeNaPopustu() {
        LocalDateTime sada = LocalDateTime.now();
        return (popust.getPocetakVazenja() != null && popust.getKrajVazenja() != null && sada.isAfter(popust.getPocetakVazenja()) && sada.isBefore(popust.getKrajVazenja()));
    }

}
