package com.travelagency.travelAgency.model;

public enum StatusRezervacije {
    NA_CEKANJU,
    PRIHVACENO,
    ODBIJENO
}
