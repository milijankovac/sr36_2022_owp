package com.travelagency.travelAgency.model;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Table(name = "korisnik")
public class Korisnik {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String email;

    private String lozinka;

    private String ime;

    private String prezime;

    @Column(name = "datum_rodjenja")
    private LocalDate datumRodjenja;

    private String jmbg;

    private String adresa;

    @Column(name = "broj_telefona")
    private Integer brojTelefona;

    @Column(name = "datum_vreme_registracije")
    private LocalDateTime datumVremeRegistracije;

    @Enumerated(EnumType.STRING)
    private Uloga uloga;

    public Korisnik(Korisnik korisnikDTO) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public LocalDate getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(LocalDate datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Integer getBrojTelefona() {
        return brojTelefona;
    }

    public void setBrojTelefona(Integer brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public LocalDateTime getDatumVremeRegistracije() {
        return datumVremeRegistracije;
    }

    public void setDatumVremeRegistracije(LocalDateTime datumVremeRegistracije) {
        this.datumVremeRegistracije = datumVremeRegistracije;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    public Korisnik() {
        super();
    }

    public Korisnik(String email, String lozinka, String ime, String prezime, LocalDate datumRodjenja, String jmbg,
                    String adresa, Integer brojTelefona, LocalDateTime datumVremeRegistracije, Uloga uloga) {
        super();
        this.email = email;
        this.lozinka = lozinka;
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodjenja = datumRodjenja;
        this.jmbg = jmbg;
        this.adresa = adresa;
        this.brojTelefona = brojTelefona;
        this.datumVremeRegistracije = datumVremeRegistracije;
        this.uloga = uloga;
    }
}
