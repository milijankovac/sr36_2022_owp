package com.travelagency.travelAgency.model;

public enum PrevoznoSredstvo {
    AVION,
    AUTOBUS,
    SOPTSVENI_PREVOZ
}
