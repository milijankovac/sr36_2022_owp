package com.travelagency.travelAgency.model;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "rezervacije")
public class Rezervacija {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "korisnik_id")
    private Korisnik korisnik;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "putovanje_id")
    private Putovanje putovanje;
    private Integer brojMesta;
    @Column(name = "ukupna_cena")
    private Double ukupnaCena;
    @Enumerated(EnumType.STRING)
    private StatusRezervacije statusRezervacije; // Dodali smo enum StatusRezervacije


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Korisnik getKorisnik() {
        return korisnik;
    }
    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Putovanje getPutovanje() {
        return putovanje;
    }

    public void setPutovanje(Putovanje putovanje) {
        this.putovanje = putovanje;
    }

    public Integer getBrojMesta() {
        return brojMesta;
    }

    public void setBrojMesta(Integer brojMesta) {
        this.brojMesta = brojMesta;
    }
    public Double getUkupnaCena() {
        return ukupnaCena;
    }

    public void setUkupnaCena(Double ukupnaCena) {
        this.ukupnaCena = ukupnaCena;
    }

    public StatusRezervacije getStatusRezervacije(){
        return statusRezervacije;
    }
    public void setStatusRezervacije(StatusRezervacije statusRezervacije){
        this.statusRezervacije = statusRezervacije;
    }

    public Rezervacija() {
        super();
    }

    public Rezervacija(Korisnik korisnik, Putovanje putovanje, Integer brojMesta, Double ukupnaCena, StatusRezervacije statusRezervacije) {
        super();
        this.korisnik = korisnik;
        this.putovanje = putovanje;
        this.brojMesta = brojMesta;
        this.ukupnaCena = ukupnaCena;
        this.statusRezervacije = statusRezervacije;
    }
    public boolean isFinished() {
        LocalDateTime datumVremePolaska = this.putovanje.getDatumVremePolaska();
        if (datumVremePolaska == null) {
            return false; // ili neka druga vrednost koja odgovara vašem slučaju
        }
        return datumVremePolaska.isBefore(LocalDateTime.now().minusDays(2));
    }

}

