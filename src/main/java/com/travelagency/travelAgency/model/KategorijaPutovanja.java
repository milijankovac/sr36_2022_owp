package com.travelagency.travelAgency.model;


import jakarta.persistence.*;

@Entity
@Table(name = "kategorija_putovanja")
public class KategorijaPutovanja {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String naziv;

    private String opis;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public KategorijaPutovanja() {
        super();
    }

    public KategorijaPutovanja(String naziv, String opis) {
        super();
        this.naziv = naziv;
        this.opis = opis;
    }
}
