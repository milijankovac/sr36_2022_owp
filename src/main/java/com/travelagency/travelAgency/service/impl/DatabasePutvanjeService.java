package com.travelagency.travelAgency.service.impl;

import com.travelagency.travelAgency.dao.PutovanjeDAO;
import com.travelagency.travelAgency.dao.RezervacijaDAO;
import com.travelagency.travelAgency.model.Putovanje;
import com.travelagency.travelAgency.service.PutovanjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DatabasePutvanjeService implements PutovanjeService {
    @Autowired
    private PutovanjeDAO putovanjeDAO;
    @Autowired
    private RezervacijaDAO rezervacijaDAO;
    @Override
    public List<Putovanje> getAll() {
        return putovanjeDAO.findAll();
    }
    @Override
    public Putovanje getById(Long id) {
        return putovanjeDAO.findById(id);
    }
    @Override
    public Putovanje save(Putovanje putovanje) {
        return putovanjeDAO.save(putovanje);
    }
    @Override
    public int delete(Long id) {
        return putovanjeDAO.delete(id);
    }
    @Override
    public List<Putovanje> findByCenaBetween(double minCena, double maxCena) {
        return putovanjeDAO.findByCenaBetween(minCena, maxCena);
    }
    @Override
    public List<Putovanje> findByCenaGreaterThanEqual(double minCena) {
        return putovanjeDAO.findByCenaGreaterThanEqual(minCena);
    }
    @Override
    public List<Putovanje> findByCenaLessThanEqual(double maxCena) {
        return putovanjeDAO.findByCenaLessThanEqual(maxCena);
    }
    @Override
    public List<Putovanje> getPutovanjaSaSlobodnimMjestima() {
        return putovanjeDAO.findByBrojSlobodnihMestaGreaterThan(0);
    }
    @Override
    public List<Putovanje> searchPutovanja(String prevoznoSredstvo, String nazivDestinacije, Long kategorijaId, Integer brojNocenja, Double minCena, Double maxCena) {
        // Implementirajte pretragu prema zadatim kriterijumima
        // Na primer:
        return putovanjeDAO.search(prevoznoSredstvo, nazivDestinacije, kategorijaId, brojNocenja, minCena, maxCena);
    }

    @Override
    public List<Putovanje> sortPutovanja(String sortBy) {
        // Implementirajte sortiranje prema zadatom kriterijumu
        // Na primer:
        return putovanjeDAO.sortBy(sortBy);
    }
    @Override
    public List<Putovanje> searchPutovanjaSaSlobodnimMjestima(String prevoznoSredstvo,String nazivDestinacije,Long kategorijaId,Integer brojNocenja,Double minCena,Double maxCena){
        List<Putovanje> putovanja = searchPutovanja(prevoznoSredstvo, nazivDestinacije, kategorijaId, brojNocenja, minCena, maxCena);
        return putovanja.stream()
                .filter(p -> p.getBrojSlobodnihMesta() > 0)
                .collect(Collectors.toList());
    }
    @Override
    public List<Putovanje> sortPutovanjaSaSlobodnimMjestima(String sortBy){
        List<Putovanje> putovanja = sortPutovanja(sortBy);
        return putovanja.stream()
                .filter(p -> p.getBrojSlobodnihMesta() > 0)
                .collect(Collectors.toList());
    }
    @Override
    public boolean hasReservations(Long putovanjeId) {
        return rezervacijaDAO.existsByPutovanjeId(putovanjeId);
    }
    @Override
    public Putovanje update(Putovanje putovanje) {
        if (putovanje.getId() == null) {
            throw new IllegalArgumentException("Putovanje mora imati postavljen ID za ažuriranje.");
        }
        return putovanjeDAO.update(putovanje);
    }
}
