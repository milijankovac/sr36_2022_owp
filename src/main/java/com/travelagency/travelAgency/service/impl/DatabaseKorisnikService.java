package com.travelagency.travelAgency.service.impl;

import com.travelagency.travelAgency.dao.KorisnikDAO;
import com.travelagency.travelAgency.model.Korisnik;
import com.travelagency.travelAgency.model.Uloga;
import com.travelagency.travelAgency.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
@Service
public class DatabaseKorisnikService implements KorisnikService {
    @Autowired
    private KorisnikDAO korisnikDAO;
    @Override
    public List<Korisnik> getAll() {
        return korisnikDAO.findAll();
    }
    @Override
    public Korisnik findById(Long id) {
        return korisnikDAO.findById(id);
    }
    @Override
    public Korisnik findByUsername(String email) {
        return korisnikDAO.findByEmail(email);
    }
    @Override
    public Korisnik findForLogin(String email, String sifra){
        return korisnikDAO.findForLogin(email, sifra);
    }
    @Override
    public void create(Korisnik korisnik) {
        //Korisnik korisnik = new Korisnik(email, lozinka, ime, prezime, datumRodjenja, jmbg, adresa, brojTelefona, datumVremeRegistracije, uloga);
        korisnikDAO.save(korisnik);
    }
    @Override
    public Korisnik update(Korisnik korisnik) {
        Korisnik updatedKorisnik = korisnikDAO.findById(korisnik.getId());

        updatedKorisnik.setEmail(korisnik.getEmail());
        updatedKorisnik.setLozinka(korisnik.getLozinka());
        updatedKorisnik.setIme(korisnik.getIme());
        updatedKorisnik.setPrezime(korisnik.getPrezime());
        updatedKorisnik.setJmbg(korisnik.getJmbg());
        updatedKorisnik.setDatumRodjenja(korisnik.getDatumRodjenja());
        updatedKorisnik.setBrojTelefona(korisnik.getBrojTelefona());
        updatedKorisnik.setAdresa(korisnik.getAdresa());

        korisnikDAO.update(korisnik);

        return updatedKorisnik;
    }
    @Override
    public void delete(Long id){
        korisnikDAO.delete(id);
    }
    public boolean updatePassword(Korisnik korisnik, String newPassword) {
        return korisnikDAO.updatePassword(korisnik, newPassword);
    }

    @Override
    public boolean checkPassword(Korisnik korisnik, String currentPassword) {
        // Trimovanje lozinki da bi se uklonile neželjene praznine
        return korisnik.getLozinka().trim().equals(currentPassword.trim());
    }



}
