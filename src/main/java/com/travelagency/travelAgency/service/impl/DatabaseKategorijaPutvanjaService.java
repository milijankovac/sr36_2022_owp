package com.travelagency.travelAgency.service.impl;

import com.travelagency.travelAgency.dao.KategorijaPutovanjaDAO;
import com.travelagency.travelAgency.model.KategorijaPutovanja;
import com.travelagency.travelAgency.service.KategorijaPutovanjaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DatabaseKategorijaPutvanjaService implements KategorijaPutovanjaService {
    @Autowired
    private KategorijaPutovanjaDAO kategorijaPutovanjaDAO;
    @Override
    public List<KategorijaPutovanja> getAllKategorije() {
        return kategorijaPutovanjaDAO.findAll();
    }
    @Override
    public KategorijaPutovanja getKategorijaById(Long id) {
        return kategorijaPutovanjaDAO.findOne(id);
    }
    @Override
    public int saveKategorija(KategorijaPutovanja kategorija) {
        return kategorijaPutovanjaDAO.save(kategorija);
    }
    @Override
    public int updateKategorija(KategorijaPutovanja kategorija) {
        return kategorijaPutovanjaDAO.update(kategorija);
    }
    @Override
    public int deleteKategorija(KategorijaPutovanja kategorija) {
        return kategorijaPutovanjaDAO.delete(kategorija);
    }
}
