package com.travelagency.travelAgency.service.impl;

import com.travelagency.travelAgency.dao.KorisnikDAO;
import com.travelagency.travelAgency.dao.PutovanjeDAO;
import com.travelagency.travelAgency.dao.RezervacijaDAO;
import com.travelagency.travelAgency.model.*;
import com.travelagency.travelAgency.service.KorisnikService;
import com.travelagency.travelAgency.service.PutovanjeService;
import com.travelagency.travelAgency.service.RezervacijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Service
public class DatabaseRezervacijaService implements RezervacijaService {
    @Autowired
    private RezervacijaDAO rezervacijaDAO;

    @Autowired
    private PutovanjeDAO putovanjeDAO;
    @Autowired
    private PutovanjeService putovanjeService;

    @Autowired
    private KorisnikDAO korisnikDAO;
    public List<Rezervacija> getAll(){
        List<Rezervacija> rezervacije = rezervacijaDAO.findAll();
        for (Rezervacija rezervacija : rezervacije) {
            Putovanje putovanje = putovanjeService.getById(rezervacija.getPutovanje().getId());
            rezervacija.setPutovanje(putovanje);
        }
        return rezervacije;
    }
    public boolean reserve(Long putovanjeId, Long korisnikId, int brojMesta) {
        Putovanje putovanje = putovanjeDAO.findById(putovanjeId);
        Korisnik korisnik = korisnikDAO.findById(korisnikId);
        Double ukupnaCena = brojMesta * putovanje.getCena();
        StatusRezervacije statusRezervacije = StatusRezervacije.NA_CEKANJU;
        if (putovanje == null || korisnik == null) {
            return false;
        }

        if (putovanje.jeNaPopustu()) {
            ukupnaCena = brojMesta * putovanje.getCena() * (1 - putovanje.getPopust().getProcenat() / 100.0);
        } else {
            ukupnaCena = brojMesta * putovanje.getCena();
        }

        if (putovanje.getBrojSlobodnihMesta() >= brojMesta) {
            putovanje.setBrojSlobodnihMesta(putovanje.getBrojSlobodnihMesta() - brojMesta);
            putovanjeDAO.save(putovanje);

            Rezervacija rezervacija = new Rezervacija(korisnik, putovanje, brojMesta, ukupnaCena, statusRezervacije);
            rezervacijaDAO.save(rezervacija);

            return true;
        } else {
            return false;
        }
    }
    @Override
    public boolean cancel(Long id) {
        Rezervacija rezervacija = findById(id);

        if (rezervacija == null || rezervacija.isFinished()) {
            return false;
        }

        Putovanje putovanje = rezervacija.getPutovanje();
        System.out.println(putovanje.getBrojSlobodnihMesta());
        rezervacijaDAO.delete(rezervacija);

        putovanje.setBrojSlobodnihMesta(putovanje.getBrojSlobodnihMesta() + rezervacija.getBrojMesta());
        putovanjeDAO.save(putovanje);

        return true;
    }
    @Override
    public List<Rezervacija> getFinished() {
        List<Rezervacija> all = rezervacijaDAO.findAll();

        List<Rezervacija> finished = new ArrayList<>();

        for (Rezervacija r : all) {
            if (r.getPutovanje().getDatumVremePolaska().isBefore(LocalDateTime.now())) {
                finished.add(r);
            }
        }

        return finished;
    }
    @Override
    public List<Rezervacija> getActive() {
        List<Rezervacija> all = rezervacijaDAO.findAll();

        List<Rezervacija> active = new ArrayList<>();

        for (Rezervacija r : all) {
            if (r.getPutovanje().getDatumVremePolaska().isAfter(LocalDateTime.now())) {
                active.add(r);
            }
        }

        return active;
    }
    @Override
    public  List<Rezervacija> existsReservation(Long putovanjeId, Long korisnikId) {
        List<Rezervacija> rezervacije = rezervacijaDAO.findByKorisnikAndPutovanje(korisnikId, putovanjeId);
        return rezervacije;
    }
    @Override
    public List<Rezervacija> getRezervacijeByKorisnik(Long korisnikId) {
        List<Rezervacija> rezervacije = rezervacijaDAO.findByKorisnik(korisnikId);
        for (Rezervacija rezervacija : rezervacije) {
            Putovanje putovanje = putovanjeService.getById(rezervacija.getPutovanje().getId());
            rezervacija.setPutovanje(putovanje);
        }
        return rezervacije;
    }
    @Override
    public Rezervacija findById(Long id) {
        Rezervacija rezervacija = rezervacijaDAO.findOne(id);
        if (rezervacija != null) {
            Putovanje putovanje = putovanjeService.getById(rezervacija.getPutovanje().getId());
            rezervacija.setPutovanje(putovanje);
        }
        return rezervacija;
    }

    @Override
    public List<Rezervacija> getFinishedRezervacijeByKorisnik(Long korisnikId) {
        List<Rezervacija> rezervacije = rezervacijaDAO.findByKorisnik(korisnikId);
        List<Rezervacija> finishedRezervacije = new ArrayList<>();

        System.out.println("Rezervacije za korisnika ID " + korisnikId + ": " + rezervacije.size());

        for (Rezervacija rezervacija : rezervacije) {
            System.out.println("Rezervacija ID " + rezervacija.getId() + " isFinished: " + rezervacija.isFinished());
            Putovanje putovanje = putovanjeService.getById(rezervacija.getPutovanje().getId());
            rezervacija.setPutovanje(putovanje);
            LocalDateTime datumVremePolaska = putovanje.getDatumVremePolaska();
            if (datumVremePolaska.isBefore(LocalDateTime.now().minusDays(2))) {

                finishedRezervacije.add(rezervacija);
            }
        }

        System.out.println("Završene rezervacije: " + finishedRezervacije.size());
        return finishedRezervacije;
    }

    @Override
    public List<Rezervacija> getUpcomingRezervacijeByKorisnik(Long korisnikId) {
        List<Rezervacija> rezervacije = rezervacijaDAO.findByKorisnik(korisnikId);
        List<Rezervacija> upcomingRezervacije = new ArrayList<>();

        System.out.println("Rezervacije za korisnika ID " + korisnikId + ": " + rezervacije.size());

        for (Rezervacija rezervacija : rezervacije) {
            System.out.println("Rezervacija ID " + rezervacija.getId() + " isFinished: " + rezervacija.isFinished());
            Putovanje putovanje = putovanjeService.getById(rezervacija.getPutovanje().getId());
            rezervacija.setPutovanje(putovanje);
            LocalDateTime datumVremePolaska = putovanje.getDatumVremePolaska();
            if (!datumVremePolaska.isBefore(LocalDateTime.now().minusDays(2))) {

                upcomingRezervacije.add(rezervacija);
            }
        }

        System.out.println("Predstojeće rezervacije: " + upcomingRezervacije.size());
        return upcomingRezervacije;
    }
    @Override
    public boolean prihvatiRezervaciju(Long rezervacijaId) {
        Rezervacija rezervacija = rezervacijaDAO.findOne(rezervacijaId);
        if (rezervacija != null && rezervacija.getStatusRezervacije() == StatusRezervacije.NA_CEKANJU) {
            rezervacija.setStatusRezervacije(StatusRezervacije.PRIHVACENO);
            rezervacijaDAO.update(rezervacija);
            return true;
        }
        return false;
    }
    @Override
    public boolean odbijRezervaciju(Long rezervacijaId) {
        Rezervacija rezervacija = rezervacijaDAO.findOne(rezervacijaId);
        if (rezervacija != null && rezervacija.getStatusRezervacije() == StatusRezervacije.NA_CEKANJU) {
            Putovanje putovanje = rezervacija.getPutovanje();
            putovanje = putovanjeService.getById(putovanje.getId());
            int brojMestaRezervisano = rezervacija.getBrojMesta();
            System.out.println(brojMestaRezervisano);

            putovanje.setBrojSlobodnihMesta(putovanje.getBrojSlobodnihMesta() + brojMestaRezervisano);

            rezervacija.setStatusRezervacije(StatusRezervacije.ODBIJENO);

            rezervacijaDAO.update(rezervacija);
            putovanjeDAO.update(putovanje);

            return true;
        }
        return false;
    }

}
