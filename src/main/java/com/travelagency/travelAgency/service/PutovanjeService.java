package com.travelagency.travelAgency.service;

import com.travelagency.travelAgency.model.Putovanje;

import java.util.List;

public interface PutovanjeService {
    List<Putovanje> getAll();
    Putovanje getById(Long id);
    Putovanje save(Putovanje putovanje);
    Putovanje update(Putovanje putovanje);
    int delete(Long id);
    List<Putovanje> findByCenaBetween(double minCena, double maxCena);
    List<Putovanje> findByCenaGreaterThanEqual(double minCena);
    List<Putovanje> findByCenaLessThanEqual(double maxCena);
    List<Putovanje> getPutovanjaSaSlobodnimMjestima();
    List<Putovanje> searchPutovanja(String prevoznoSredstvo, String nazivDestinacije, Long kategorijaId, Integer brojNocenja, Double minCena, Double maxCena);
    List<Putovanje> sortPutovanja(String sortBy);
    List<Putovanje> searchPutovanjaSaSlobodnimMjestima(String prevoznoSredstvo,String nazivDestinacije,Long kategorijaId,Integer brojNocenja,Double minCena,Double maxCena);
    List<Putovanje> sortPutovanjaSaSlobodnimMjestima(String sortBy);
    boolean hasReservations(Long putovanjeId);
}
