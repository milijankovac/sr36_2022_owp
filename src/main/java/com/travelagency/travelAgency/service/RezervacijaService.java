package com.travelagency.travelAgency.service;

import com.travelagency.travelAgency.model.Rezervacija;

import java.util.List;

public interface RezervacijaService {
    boolean reserve(Long putovanjeId, Long korisnikId, int brojMesta);
    Rezervacija findById(Long id);
    boolean cancel(Long id);
    List<Rezervacija> getFinished();
    List<Rezervacija> getActive();
    List<Rezervacija> existsReservation(Long putovanjeId, Long korisnikId);
    List<Rezervacija> getRezervacijeByKorisnik(Long korisnikId);
    List<Rezervacija> getAll();
    List<Rezervacija> getFinishedRezervacijeByKorisnik(Long korisnikId);
    List<Rezervacija> getUpcomingRezervacijeByKorisnik(Long korisnikId);
    boolean prihvatiRezervaciju(Long rezervacijaId);
    boolean odbijRezervaciju(Long rezervacijaId);
}
