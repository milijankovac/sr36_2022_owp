package com.travelagency.travelAgency.service;

import com.travelagency.travelAgency.model.Korisnik;

import java.util.List;

public interface KorisnikService {
    List<Korisnik> getAll();
    Korisnik findById(Long id);
    Korisnik findByUsername(String email);
    Korisnik findForLogin(String email, String sifra);
    void create(Korisnik korisnik);
    Korisnik update(Korisnik korisnik);
    void delete(Long id);
    boolean updatePassword(Korisnik korisnik, String novaLozinka);
    boolean checkPassword(Korisnik korisnik, String currentPassword);


}
