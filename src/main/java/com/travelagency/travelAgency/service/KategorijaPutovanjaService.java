package com.travelagency.travelAgency.service;

import com.travelagency.travelAgency.model.KategorijaPutovanja;

import java.util.List;

public interface KategorijaPutovanjaService {
    List<KategorijaPutovanja> getAllKategorije();
    KategorijaPutovanja getKategorijaById(Long id);
    int saveKategorija(KategorijaPutovanja kategorija);
    int updateKategorija(KategorijaPutovanja kategorija);
    int deleteKategorija(KategorijaPutovanja kategorija);
}
