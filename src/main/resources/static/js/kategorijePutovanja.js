$(document).ready(function() {
    $.ajax({
        url: '/putovanja/kategorije',
        method: 'GET',
        success: function(data) {
            var select = $('#kategorijaPutovanja');
            data.forEach(function(kategorija) {
                select.append('<option value="' + kategorija.id + '">' + kategorija.naziv + '</option>');
            });
        },
        error: function(error) {
            console.error('Error fetching categories:', error);
        }
    });
});
