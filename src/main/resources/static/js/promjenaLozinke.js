$(document).ready(function() {
    $('#changePasswordForm').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
                $('#changePasswordModal').modal('hide');  // Zatvori modal
                toastr.success('Lozinka uspješno promijenjena');  // Prikazi toastr poruku
            },
            error: function(xhr, status, error) {
                // Obrada grešaka ako je potrebno
                toastr.error('Došlo je do greške prilikom promjene lozinke');
            }
        });
    });
});
